
import model.Person;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
public class MySqlDaoTest {

    MySqlDao mySqlDao = new MySqlDao();

    @Test
    public void read() {
        int size = mySqlDao.read().size();
        Assert.assertEquals(4,size);
    }

    @Test
    public void count() {
        int sizeAfter = mySqlDao.read().size();
        int sizeBefore = mySqlDao.count();

        Assert.assertEquals(sizeAfter,sizeBefore);
    }

    @Test
    public void middleAge() {
        int midleAge = mySqlDao.read().size();
        Assert.assertEquals(31,midleAge);
    }

    @Test
    public void lastNameOnAlfavit() {

        String nameOne = mySqlDao.lastNameOnAlfavit().get(0);

        String nameTwo = mySqlDao.lastNameOnAlfavit().get(1);

        assertEquals("Baranets", nameOne);
        assertEquals("Pilatov", nameTwo);
    }

    @Test
    public void repetitionOfSurnames() {
        Map map = mySqlDao.repetitionOfSurnames();

        assertEquals(1, map.get("Pilatov"));
        assertEquals(1, map.get("Var"));
        assertEquals(1, map.get("Baranets"));
    }

    @Test
    public void containsLastNameR() {
        String nameO = mySqlDao.lastNameOnAlfavit().get(0);
        assertEquals("Baranets", nameO);
    }

    @Test
    public void readBum() {
        List<Person> listBum = mySqlDao.readBum();

        assertNull(listBum.get(0).getStreet().getName());
    }

    @Test
    public void minorsOnThePririth() {
        List<Person> listBum = mySqlDao.minorsOnThePririth();
        assertEquals("pririt", listBum
                .get(0)
                .getStreet()
                .getName());
        assertTrue(listBum.get(0).getAge()<18);
    }

    @Test
    public void chewsOnTheStreet() {
        Map map = mySqlDao.chewsOnTheStreet();

        assertEquals(1, map.get(null));
        assertEquals(3, map.get("pririt"));

    }

    @Test
    public void streetSixNumber() {
        List<String> streetSixNumber = mySqlDao.streetSixNumber();

        assertEquals("geroiv", streetSixNumber.get(0));
        assertEquals("pririt", streetSixNumber.get(1));

    }

    @Test
    public void listOfStreetWichFewerThanThreePerson() {
        Map map = mySqlDao.listOfStreetWichFewerThanThreePerson();

        assertEquals(1, map.get(null));

    }
}
