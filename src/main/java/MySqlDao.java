import model.Person;
import model.Street;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySqlDao {

    private List<Person> people = new ArrayList<Person>();

    private Connection getDBConnection() {
        return MySqlUtil.getDBConnection();
    }
    private Connection c = getDBConnection();

    List read(){

        people.clear();
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT persona.id," +
                            "persona.firstName," +
                            "persona.lastName," +
                            "persona.age," +
                            "persona.idStreet," +
                            "street.name" +
                            " FROM person.persona,person.street" +
                            " WHERE persona.idStreet = street.id");

            while (set.next()) {
                people.add(new Person(
                        set.getInt(1),
                        set.getString(2),
                        set.getString(3),
                        set.getInt(4),
                        new Street(set.getInt(5),set.getString(6))
                ));
            }
            statement.close();

            for (Person p : people) {
                System.out.println("Получена запись: " + p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return people;
    }


    int count(){

        int count =0;
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT COUNT(*) as count FROM person.persona");

            set.next();
            count=set.getInt(1);

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count;
    }

    int middleAge(){
        int age =0;
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT AVG(age) as avg FROM person.persona");

            set.next();
            age=set.getInt(1);

            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return age;
    }

    List<String> lastNameOnAlfavit(){

        List<String> lastNamesOnAlfavit = new ArrayList<String>();

        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT lastName FROM  person.persona ORDER BY lastName"
            );

            while (set.next()) {
                lastNamesOnAlfavit.add(set.getString(1));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lastNamesOnAlfavit;
    }

    Map<String,Integer> repetitionOfSurnames(){

        Map<String,Integer> repetitionOfSurnames = new HashMap<String, Integer>();
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT lastName,COUNT(lastName) as size FROM person.persona GROUP BY lastName"
            );

            while (set.next()) {
                repetitionOfSurnames.put(set.getString(1),set.getInt(2));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return repetitionOfSurnames;
    }

    List<String> containsLastNameR(){

        List<String> containsLastNameB = new ArrayList<String>();

        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT lastName FROM  person.persona WHERE lastName LIKE '%_r_%'"
            );

            while (set.next()) {
                containsLastNameB.add(set.getString(1));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return containsLastNameB;
    }

    List readBum(){

        people.clear();
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT persona.id," +
                            "persona.firstName," +
                            "persona.lastName," +
                            "persona.age," +
                            "persona.idStreet," +
                            "street.name" +
                            " FROM person.persona,person.street" +
                            " WHERE persona.idStreet = street.id AND street.name IS NULL");
            while (set.next()) {
                people.add(new Person(
                        set.getInt(1),
                        set.getString(2),
                        set.getString(3),
                        set.getInt(4),
                        new Street(set.getInt(5),set.getString(6))
                ));
            }
            statement.close();

            for (Person p : people) {
                System.out.println("Получена запись: " + p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return people;
    }

    List minorsOnThePririth(){

        people.clear();
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT persona.id," +
                            "persona.firstName," +
                            "persona.lastName," +
                            "persona.age," +
                            "persona.idStreet," +
                            "street.name" +
                            " FROM person.persona,person.street" +
                            " WHERE persona.idStreet = street.id AND street.name = 'pririt' AND persona.age<18");
            while (set.next()) {
                people.add(new Person(
                        set.getInt(1),
                        set.getString(2),
                        set.getString(3),
                        set.getInt(4),
                        new Street(set.getInt(5),set.getString(6))
                ));
            }
            statement.close();

            for (Person p : people) {
                System.out.println("Получена запись: " + p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return people;
    }


    Map<String,Integer> chewsOnTheStreet(){

        Map<String,Integer> chewsOnTheStreet = new HashMap<String, Integer>();
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT street.name," +
                            "COUNT(persona.lastName) as size " +
                            "FROM person.street,person.persona " +
                            "WHERE street.id = persona.idStreet " +
                            "GROUP BY street.name");

            while (set.next()) {
                chewsOnTheStreet.put(set.getString(1),set.getInt(2));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return chewsOnTheStreet;
    }

    List<String> streetSixNumber(){

        List<String> streetSixNumber = new ArrayList<String>();

        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT name FROM  person.street WHERE name LIKE '______'"
            );

            while (set.next()) {
                streetSixNumber.add(set.getString(1));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return streetSixNumber;
    }

    Map<String,Integer> listOfStreetWichFewerThanThreePerson(){

        Map<String,Integer> chewsOnTheStreet = new HashMap<String, Integer>();
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT street.name," +
                            "COUNT(persona.lastName) AS count " +
                            "FROM person.street,person.persona " +
                            "WHERE street.id = persona.idStreet " +
                            "GROUP BY street.name "+
                            "HAVING COUNT(persona.lastName)<3 "
            );

            while (set.next()) {
                chewsOnTheStreet.put(set.getString(1),set.getInt(2));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return chewsOnTheStreet;
    }
}
