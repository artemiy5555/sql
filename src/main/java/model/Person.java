package model;

public class Person {

    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private Street Street;

    public Person(int id, String firstName, String lastName, int age, Street street) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        Street = street;
    }

    public Person() {
    }

    public model.Street getStreet() {
        return Street;
    }

    public void setStreet(model.Street street) {
        Street = street;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age='" + age + '\'' +
                ", Street=" + Street.toString() +
                '}';
    }
}
